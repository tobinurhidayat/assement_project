package com.xapos.batch290;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XaposBatch290MiniproApplication {

	public static void main(String[] args) {
		SpringApplication.run(XaposBatch290MiniproApplication.class, args);
	}

}
