package com.xapos.batch290.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos.batch290.model.Blood;
import com.xapos.batch290.repository.BloodRepository;

@Controller
@RequestMapping("/blood/")
public class BloodController {
	
	@Autowired
	public BloodRepository bloodRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("blood/indexapi");
		return view;
	}
	
	@GetMapping("layout")
	public ModelAndView layout() {
		ModelAndView view = new ModelAndView("fragment/layout");
		return view;
	}
}
