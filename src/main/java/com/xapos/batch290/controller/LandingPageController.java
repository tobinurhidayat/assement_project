package com.xapos.batch290.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/fragment/")
public class LandingPageController {

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("fragment/index");
		return view;
	}
	
	@GetMapping("indexafterapi")
	public ModelAndView indexafterapi() {
		ModelAndView view = new ModelAndView("fragment/indexafterapi");
		return view;
	}
	
	@GetMapping("indexpasien")
	public ModelAndView indexpasien() {
		ModelAndView view = new ModelAndView("fragment/indexpasien");
		return view;
	}
	
	@GetMapping("indexdokter")
	public ModelAndView indexdokter() {
		ModelAndView view = new ModelAndView("fragment/indexdokter");
		return view;
	}
}
