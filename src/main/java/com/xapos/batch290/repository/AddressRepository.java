package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos.batch290.model.AddressModel;



public interface AddressRepository extends JpaRepository<AddressModel, Long> {

	List<AddressModel> findByIsDelete(Boolean isDelete);
}
