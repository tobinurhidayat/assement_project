package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos.batch290.model.BiodataModel;



public interface BiodataRepository extends JpaRepository<BiodataModel, Long> {

	List<BiodataModel> findByIsDelete(Boolean isDelete);
	
	List<BiodataModel> findByFullName(String fullName);
	
	@Query("SELECT MAX(id) FROM BiodataModel")
	public Long findByMaxId();
}
