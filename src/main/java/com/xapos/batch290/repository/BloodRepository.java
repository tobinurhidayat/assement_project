package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos.batch290.model.Blood;

public interface BloodRepository extends JpaRepository<Blood, Long> {

	
	List<Blood> findByIsDelete(Boolean isDelete);
	
	@Query(value = "FROM Blood WHERE code =?1 AND  isDelete=false")
	List<Blood> findByCode(String code);
	
	@Query("FROM Blood WHERE lower(code) LIKE lower(concat('%', ?1, '%')) AND isDelete=false")
	List<Blood> searchKeyword(String keyword);
}
