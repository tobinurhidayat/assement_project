package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos.batch290.model.LocationModel;



public interface LocationRepository extends JpaRepository<LocationModel, Long> {

	List<LocationModel> findByIsDelete(Boolean isDelete);
}
